<?php

namespace App\Http\Controllers;

use App\Models\Course;
use Inertia\Inertia;
use Illuminate\Database\Eloquent\Builder;


class CourseController extends Controller
{
    //
    public function index () 
    {
        
        $courses = Course::with('user')->get();

        //dd($courses); // eloquent va ns rammener le user que si on l'utilise
        

        // on peux aussi recupérer 1 info specifique à 1 episode
        $courses = Course::with('user')->withCount('episodes')->get();
        

        return Inertia::render('Courses/Index', [
            'courses' => $courses
        ]);
    } 

    // je récupère l'entier id
//je créé 1 variable course qui contiendra l'id des jeu (ou formations selon l'usage final), j'ai aussi besoin des épisodes où je récupère le 1er enregistrement, et avec le render je le rend ds ma vue
    public function show(int $id)
    {
        $course = Course::where('id', $id)->with('episodes')->first();
        //dd($course);
        return Inertia::render('Courses/Show', [ 
            'course' => $course    
        ]);// ça s'appellera 'course' & ça contiendra ce qu'il y a ds la variable 'course'// donc j'envoie ds mes propriété course qui contiendra ce que je veux (formation ou jeu & les episodes)
    }
}
