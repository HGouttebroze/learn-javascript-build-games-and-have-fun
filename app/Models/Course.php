<?php

namespace App\Models;

use App\Models\User;
use App\Models\Episode;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Course extends Model
{
    use HasFactory;

    public function episodes() 
    {   
        // au niveau du 'course', on a plusieurs 'épisodes'    
        return $this->hasMany(Episode::class); 
    }

    public function user() 
    {
        // on va recupérer le 'user' qui a créé la formation
        return $this->belongsTo(User::class);
    }
}
